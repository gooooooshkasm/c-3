﻿
#include <iostream>
#include <string>

using namespace std;

int main()
{
    const int N = 100;
    int i;
    for (i = 0; i < N; i+=2)
    {
        std::cout << i << '\n';
    }

    return 0;
}